<?php

ob_start();

// only require the file once
// require_once throw fatal error and stop the code
// include throw a warning and continue code execution 

require_once '../public/config/db_config.php';
require_once '../public/header.php';
require_once '../public/sidebar.php';

require_once 'crud/select.php';
require_once 'crud/update.php';
require_once 'crud/function.php';

$alert_msg = '';


if (isset($_GET['id'])) {
  # code...
  $employee_id = $_GET['id'];
  $employee = get_employee_by_id($employee_id);
  foreach ($employee as $key => $value) {
    # code...
    $$key = $value; // key becomes the variable
  }
}
if (isset($_POST['update_employee'])) {
  # code...
  $totalDec =  (float)$_POST['gsis'] +  (float)$_POST['wtax'] +  (float)$_POST['loans'];
  $netPay   =  (float)$_POST['basic'] - $totalDec;

  $data = array(
    'office_id' => $_POST['office'],
    'full_name' => $_POST['full_name'],
    'position' => $_POST['position'],
    'basic' => $_POST['basic'],
    'gsis' => $_POST['gsis'],
    'wtax' => $_POST['wtax'],
    'loans' => $_POST['loans'],
    'totaldec' => $totalDec,
    'net_pay' => $netPay,
    'id'  => $employee_id
  );
  // format($data);
  $employee_updated = update_employee($data);
  if ($employee_updated) {
    // header('Location: employees');
    // exit;
  } else {
    $alert_msg .= ' 
          <div class="new-alert new-alert-warning alert-dismissible">
              <i class="icon fa fa-info"></i>
              Invalid Update.
          </div>
      ';
  }
}

$offices = fetch_offices();
$positions = fetch_positions();


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 style="border-bottom: 3px solid #bbb;">
      <b>UPDATE EMPLOYEE</b>
      <!-- <small>Version 2.0</small> -->
    </h1>
    <?php echo $alert_msg; ?>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Employee Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" action="<?php htmlspecialchars("PHP_SELF"); ?>">
            <div class="box-body">

              <div class="row">

                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Name</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="full_name" placeholder="Full name" value="<?= $full_name ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Office</label>
                </div>
                <div class="col-md-10">
                  <select name="office" id="office" class="form-control">
                    <option <?= isset($office_id) ? '' : 'selected' ?> disabled value=""> Select </option>
                    <?php foreach ($offices as $office) { ?>
                      <option <?= ($office['id'] == $office_id) ? 'selected' : '' ?> value="<?= ($office['id'] == $office_id) ? $office_id : $office['id'] ?>"><?= $office['name'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Position</label>
                </div>
                <div class="col-md-10">
                  <select name="position" id="position" class="form-control">
                    <option <?= isset($position) ? '' : 'selected' ?> disabled value=""> Select </option>
                    <?php foreach ($positions as $pos) { ?>
                      <option <?= ($pos['name'] == $position) ? 'selected' : '' ?> value="<?= ($pos['name'] == $position) ? $position : $pos['name'] ?>"><?= $pos['name'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Basic</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="basic" placeholder="Basic" value="<?= $basic ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>GSIS</label>
                </div>
                <div class="col-md-10">
                  <input type="number" class="form-control" name="gsis" placeholder="GSIS" value="<?= $gsis ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>WTax</label>
                </div>
                <div class="col-md-10">
                  <input type="number" class="form-control" name="wtax" placeholder="WTax" value="<?= $wtax ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Loans</label>
                </div>
                <div class="col-md-10">
                  <input type="number" class="form-control" name="loans" placeholder="Loans" value="<?= $loans ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Totaldec</label>
                </div>
                <div class="col-md-10">
                  <input disabled type="number" class="form-control" name="totaldec" placeholder="Totaldec" value="<?= $totaldec ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Net Pay</label>
                </div>
                <div class="col-md-10">
                  <input disabled type="number" class="form-control" name="net_pay" placeholder="Net Pay" value="<?= $net_pay ?>" required>
                </div>
              </div><br>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <input type="submit" name="update_employee" class="btn btn-primary" value="Save">
              <a href="employees">
                <input type="button" name="cancel" class="btn btn-default" value="Cancel">
              </a>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-1"></div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
require_once '../public/footer.php';
?>