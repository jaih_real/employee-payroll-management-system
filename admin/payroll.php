<?php

// only require the file once
// require_once throw fatal error and stop the code
// include throw a warning and continue code execution 

require_once '../public/config/db_config.php';
require_once '../public/header.php';
require_once '../public/sidebar.php';

require_once 'crud/select.php';
require_once 'crud/delete.php';

$alert_msg = '';

if (isset($_POST['delete_payroll'])) {
  $checkId = check_office_id($_POST['payroll_id']);
  if ($checkId) {
    $alert_msg .= ' 
        <div class="new-alert new-alert-warning alert-dismissible">
            <i class="icon fa fa-info"></i>
            Office is still being used!
        </div>
      ';
  } else {
    $delete  = delete_payroll($_POST['payroll_id']);
    if ($delete) {
      $alert_msg .= ' 
          <div class="new-alert new-alert-warning alert-dismissible">
              <i class="icon fa fa-info"></i>
              Office deleted!
          </div>
        ';
    }
  }
}
$payrolls  = fetch_offices();

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 style="border-bottom: 3px solid #bbb;">
      <b> PAYROLLS </b>
      <!-- <small>Version 2.0</small> -->
    </h1>
    <center>
      <?php
      if (isset($_POST['delete_payroll'])) {
        echo $alert_msg;
      }
      ?>
    </center>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Payroll Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" action="<?php htmlspecialchars("PHP_SELF"); ?>">
            <div class="box-body">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Pay Code</th>
                    <th>Office Name</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($payrolls as $payroll) {
                  ?>
                    <tr>
                      <td><?= $payroll['pay_code'] ?></td>
                      <td><?= $payroll['name'] ?></td>
                      <td>
                        <a class="btn btn-outline-success btn-xs" href="update_payroll.php?id=<?= $payroll['id'] ?>"><i class="fa fa-check-square-o"></i>
                        </a>
                        &nbsp;
                        <button class="btn btn-outline-danger btn-xs" data-role="confirm_delete_payroll" data-payrollid="<?= $payroll["id"] ?>"><i class="fa fa-trash-o"></i></button>
                        &nbsp;
                        <a class="btn btn-outline-primary btn-xs" href="../reports/pdf.php?key=payroll&id=<?= $payroll['id'] ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>PAYROLL</a>
                        <a class="btn btn-outline-primary btn-xs" href="../reports/pdf.php?key=payslip&id=<?= $payroll['id'] ?>" target="_blank"><i class="fa fa-file-pdf-o"></i>PAYSLIP</a>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-1"></div>
    </div>

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->

<!-- modals here -->
<!-- modal here delete -->
<div class="modal fade" id="delete_payroll-form" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <form method="POST" action="">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Delete Record?</label>
              <input type="hidden" name="payroll_id" id="payroll_id" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left bg-olive" data-dismiss="modal">No</button>
          <input type="submit" name="delete_payroll" class="btn btn-danger" value="Yes">
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php

require_once '../public/footer.php';

?>