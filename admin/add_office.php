<?php
  
  // only require the file once
  // require_once throw fatal error and stop the code
  // include throw a warning and continue code execution 
  require_once '../public/config/db_config.php';
  require_once '../public/header.php';
  require_once '../public/sidebar.php';
  require_once 'crud/insert.php';
  require_once 'crud/select.php';

  $alert_msg = '';

  if (isset($_POST['insert_offices'])) {
    $data = array(
      'pay_code' => $_POST['pay_code'],
      'name' => $_POST['name']
    );

    $insert_data = insert_office($data);
    if ($insert_data) {
      # code...
      $alert_msg .= ' 
          <div class="new-alert new-alert-success alert-dismissible">
              <i class="icon fa fa-info"></i>
              Data Inserted.
          </div>
      ';
    }
    else{
      $alert_msg .= ' 
          <div class="new-alert new-alert-warning alert-dismissible">
              <i class="icon fa fa-info"></i>
              Invalid Data.
          </div>
      ';
    }
  }

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Office
        <!-- <small>Version 2.0</small> -->
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Office Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="<?php htmlspecialchars("PHP_SELF");?>">
              <div class="box-body">

                <?php echo $alert_msg; ?>

                <div class="row">
                  <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                   <label>Pay Code</label>
                  </div>
                  <div class="col-md-10">
                      <input type="text" class="form-control" name="pay_code" placeholder="Pay Code" value="" required>
                  </div>
                </div><br>
                <div class="row">
                  <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                   <label>Office Name</label>
                  </div>
                  <div class="col-md-10">
                      <input type="text" class="form-control" name="name" placeholder="Office Name" value="" required>
                  </div>
                </div><br>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <input type="submit" name="insert_offices" class="btn btn-primary" value="Save">
                <a href="offices">
                  <input type="button" name="back" class="btn btn-default" value="Back ">       
                </a>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-1"></div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<?php

  require_once '../public/footer.php';


?>