<?php

// only require the file once
// require_once throw fatal error and stop the code
// include throw a warning and continue code execution 

require_once '../public/config/db_config.php';
require_once '../public/header.php';
require_once '../public/sidebar.php';

require_once 'crud/select.php';
require_once 'crud/delete.php';

$alert_msg = '';

if (isset($_POST['delete_office'])) {
  
  $delete  = delete_office($_POST['office_id']);
  if ($delete) {

  }
}
$offices  = fetch_offices();

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 style="border-bottom: 3px solid #bbb;">
      <b> OFFICES </b>
      <!-- <small>Version 2.0</small> -->
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Office Details</h3>
            <a href="../reports/pdf.php?key=offices" style="float: right" target="_blank">
              <button class="btn btn-sm btn-primary btn-block margin-bottom">
                Generate PDF
              </button>
            </a>
            <a href="add_office" style="float:right; margin-right:10px">
              <button class="btn btn-sm btn-primary btn-block margin-bottom">
                Add Office
              </button>
            </a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" action="<?php htmlspecialchars("PHP_SELF"); ?>">
            <div class="box-body">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Pay Code</th>
                    <th>Office Name</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $counter = 0;
                  foreach ($offices as $office) {
                    $counter++;
                  ?>
                    <tr>
                      <td><?= $counter ?></td>
                      <td><?= $office['pay_code'] ?></td>
                      <td><?= $office['name'] ?></td>
                      <td>
                        <a class="btn btn-outline-success btn-xs" href="update_office.php?id=<?= $office['id'] ?>"><i class="fa fa-check-square-o"></i>
                        </a>
                        &nbsp;
                        <button class="btn btn-outline-danger btn-xs" data-role="confirm_delete_office" data-officeid="<?= $office["id"] ?>"><i class="fa fa-trash-o"></i>Delete</button>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-1"></div>
    </div>

  </section>
  <!-- /.content -->


</div>
<!-- /.content-wrapper -->

<!-- modals here -->
<!-- modal here delete -->
<div class="modal fade" id="delete_office-form" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <form method="POST" action="">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Delete Record?</label>
              <input type="hidden" name="office_id" id="office_id" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left bg-olive" data-dismiss="modal">No</button>
          <input type="submit" name="delete_office" class="btn btn-danger" value="Yes">
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php

  require_once '../public/footer.php';

?>