<?php
	
	// fetch means get only 1 array of result
	// fetchAll means get all array of data

	// fetch employee
	function fetch_employees(){
		global $con;
		try {
			$sql = 'SELECT * FROM employees ORDER BY updated_at DESC';
			$statement = $con->prepare($sql);
			$statement->execute();
			return $statement->fetchAll(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			echo 'A PDO Exeption happened';
		}
	}

	// fetch employee by id
	function get_employee_by_id($id){
		global $con;
		try {
			$sql = 'SELECT * FROM employees WHERE id = :id';
			$statement = $con->prepare($sql);
			$statement->execute([':id'=>$id]);
			return $statement->fetch(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			echo 'A PDO Exeption happened';
		}
	}

	// fetch offices
	function fetch_offices(){
		global $con;
		try {
			$sql = 'SELECT * FROM offices';
			$statement = $con->prepare($sql);
			$statement->execute();
			return $statement->fetchAll(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			// echo 'A PDO Exeption happened';
			echo $e;
		}
	}

		// fetch positions
	function fetch_positions(){
		global $con;
		try {
			$sql = 'SELECT * FROM offices';
			$statement = $con->prepare($sql);
			$statement->execute();
			return $statement->fetchAll(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			// echo 'A PDO Exeption happened';
			echo $e;
		}
	}

	// fetch offices by id
	function get_office_by_id($id)
	{
		global $con;
		try {
			$sql = 'SELECT * FROM offices WHERE id = :id';
			$statement = $con->prepare($sql);
			$statement->execute([':id' => $id]);
			return $statement->fetch(PDO::FETCH_ASSOC);
		} catch (Exception $e) {
			// echo 'A PDO Exeption happened';
			echo $e;
		}
	}
	
	// fetch payroll by id
	function get_payroll_by_id($id) // need office_id
	{
		global $con;
		try {
			$sql = 'SELECT * FROM offices WHERE id = :id';
			$statement = $con->prepare($sql);
			$statement->execute([':id' => $id]);
			return $statement->fetch(PDO::FETCH_ASSOC);
		} catch (Exception $e) {
			// echo 'A PDO Exeption happened';
			echo $e;
		}
	}

	// fetch all employee that has the same office_id
	function fetch_all_office_employees($id) // need office_id
	{
		global $con;
		try {
			$sql = 'SELECT * FROM employees WHERE office_id = :id';
			$statement = $con->prepare($sql);
			$statement->execute([':id' => $id]);
			return $statement->fetchAll(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			// echo 'A PDO Exeption happened';
			echo $e;
		}
	}

	// fetch employee that has the same employee_id
	function fetch_office_employee($id) // needs employee_id
	{
		global $con;
		try {
			$sql = 'SELECT * FROM offices 
							INNER JOIN employees 
							ON employees.office_id = offices.id 
							WHERE employees.id = :id';
			$statement = $con->prepare($sql);
			$statement->execute([':id' => $id]);
			return $statement->fetch(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			// echo 'A PDO Exeption happened';
			echo $e;
		}
	}

	// fetch payslip
	function fetch_payslip(){
		global $con;
		try {
			$sql = 'SELECT *
							FROM  offices
							INNER JOIN employees
							ON employees.office_id = offices.id';
			$statement = $con->prepare($sql);
			$statement->execute();
			return $statement->fetchAll(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			echo 'A PDO Exeption happened';
		}
	}

	// fetch payslip by id
	function get_payslip_by_id($id){
		global $con;
		try {
			$sql = 'SELECT *
							FROM  offices
							INNER JOIN employees
							ON employees.office_id = offices.id 
							WHERE offices.id = :id';
			$statement = $con->prepare($sql);
			$statement->execute([':id'=>$id]);
			return $statement->fetch(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			echo 'A PDO Exeption happened';
		}
	}

		// fetch payslip by id
	function get_payslip_pdf($id){
		global $con;
		try {
			$sql = 'SELECT *
							FROM  employees
							INNER JOIN offices
							ON employees.office_id = offices.id 
							WHERE offices.id = :id';
			$statement = $con->prepare($sql);
			$statement->execute([':id'=>$id]);
			return $statement->fetchAll(PDO::FETCH_ASSOC);
			
		} catch (Exception $e) {
			echo 'A PDO Exeption happened';
		}
	}

	// check if office_id is used in employee table
	function check_office_id($id){
		global $con;
		try {
			$sql = 'SELECT * FROM employees WHERE office_id = :id';
			$statement = $con->prepare($sql);
			$statement->execute([':id'=>$id]);
			return $statement->fetch(PDO::FETCH_ASSOC);

		} catch (\Throwable $th) {
			//throw $th;
		}
	}