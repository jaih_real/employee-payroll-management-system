<?php

	function update_employee($employee){
		global $con;

		try {
			$sql = 'UPDATE employees SET full_name = :full_name, office_id = :office_id, position = :position, basic = :basic, gsis = :gsis, wtax = :wtax, loans = :loans, totaldec = :totaldec, net_pay = :net_pay 
							WHERE id = :id';
			$statement = $con->prepare($sql);
			return $statement->execute($employee);

		} catch (Exception $e) {
			echo 'A pdo exception happened.';
		}
	}

	function update_office($office){
		global $con;

		try {
			$sql = 'UPDATE offices SET pay_code = :pay_code, name = :name WHERE id = :id';
			$statement = $con->prepare($sql);
			return $statement->execute($office);
			
		} catch (Exception $e) {
			echo 'A pdo exception happened.';
		}
	}

	function update_payroll($payroll){
		global $con;

		try {
			$sql = 'UPDATE offices SET name = :name WHERE id = :id';
			$statement = $con->prepare($sql);
			return $statement->execute($payroll);
			
		} catch (Exception $e) {
			echo 'A pdo exception happened.';
		}
	}

	function update_payslip($payslip){
		global $con;

		try {
			$sql = 'UPDATE employees 
							SET full_name = :full_name, position = :position, basic = :basic, gsis = :gsis, wtax = :wtax, loans = :loans, totaldec = :totaldec, net_pay = :net_pay 
							WHERE id = :id';
			$statement = $con->prepare($sql);
			return $statement->execute($payslip);

		} catch (Exception $e) {
			echo 'A pdo exception happened.';
		}
	}