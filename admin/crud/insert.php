<?php

	function insert_office($office){
		global $con;
		try {


			$sql = 'INSERT INTO offices(pay_code, name) VALUES(:pay_code, :name)';
			$statement = $con->prepare($sql);
			return $statement->execute($office);

		} catch (Exception $e) {
			// echo 'A pdo exception happened.';
			echo $e;
		}
	}

	function insert_employee($employee){
		global $con;
		try {


			$sql = 'INSERT INTO employees(full_name, office_id, position, basic, gsis, wtax, loans, totaldec, net_pay)  
							VALUES(:full_name, :office_id, :position, :basic, :gsis, :wtax , :loans, :totaldec, :net_pay)';
			$statement = $con->prepare($sql);
			$statement->execute($employee); 
			return $con->lastInsertId();

		} catch (Exception $e) {
			// echo 'A pdo exception happened.';
			echo $e;
		}
	}
	