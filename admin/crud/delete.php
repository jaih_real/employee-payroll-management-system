<?php

	function delete_employee($id){
		global $con;
		try {

			$sql = "DELETE FROM employees WHERE id = :id";
			$statement = $con->prepare($sql);
			return $statement->execute([':id'=>$id]);

		} catch (Exception $e) {
			// echo 'A pdo exception happened.';
			echo $e;
		}
	}

	function delete_office($id){
		global $con;
		try {

			$sql = "DELETE FROM offices WHERE id = :id";
			$statement = $con->prepare($sql);
			return $statement->execute([':id'=>$id]);

		} catch (Exception $e) {
			// echo 'A pdo exception happened.';
			echo $e;
		}

	}

	function delete_payroll($id){
		global $con;
		try {

			$sql = "DELETE FROM offices WHERE id = :id";
			$statement = $con->prepare($sql);
			return $statement->execute([':id'=>$id]);

		} catch (Exception $e) {
			// echo 'A pdo exception happened.';
			echo $e;
		}

	}