<?php
  // pdf report
  function payroll_datatable($office_id){
    $employees = fetch_all_office_employees($office_id);
    $office = get_office_by_id($office_id);
    $tbl = '';
    $tbl .= '  
      <div style="width:100%;"> 
        <table  cellpadding="5" cellspacing="1" border="1" style="text-align:center;"> 
          <tr>
            <th style="width:5%;">NO.</th>
            <th style="width:5%;">EMP ID</th>
            <th style="width:15%;">NAME</th>
            <th style="width:15%;">POSITION</th>
            <th style="width:8%;">BASIC</th>
            <th style="width:8%;">GSIS</th>
            <th style="width:8%;">WTAX</th>
            <th style="width:8%;">LOANS</th>
            <th style="width:8%;">TOTALDEC</th>
            <th style="width:10%;">NETPAY</th>
            <th style="width:10%;">REMARKS</th>
          </tr>
      <tbody>
    ';
    $totalBasic = 0;
    $totalGsis = 0;
    $totalWtax = 0;
    $totalLoans = 0;
    $totalDed = 0;
    $totalNetPay = 0;
    $counter = 0;
    // foreach start
    foreach ($employees as $employee) {
      $totalBasic += $employee['basic'];
      $totalGsis += $employee['gsis'];
      $totalWtax += $employee['wtax'];
      $totalLoans += $employee['loans'];
      $totalDed += $employee['totaldec'];
      $totalNetPay += $employee['net_pay'];
      $counter ++;
      $tbl .= '
        <tr>
          <td align="left">'.$counter. '</td>
          <td align="left">' . $employee['id'] . '</td>
          <td align="left">' . $employee['full_name'] . '</td>
          <td align="left">'.$employee['position']. '</td>
          <td align="right">'.number_format($employee['basic'],2). '</td>
          <td align="right">'.number_format($employee['gsis'],2). '</td>
          <td align="right">'.number_format($employee['wtax'],2). '</td>
          <td align="right">'.number_format($employee['loans'],2). '</td>
          <td align="right">'. number_format($employee['totaldec'], 2). '</td>
          <td align="right">'. number_format($employee['net_pay'], 2). '</td>
          <td align="right">'.'credited to atm'.'</td>
        </tr>
      ';
    } // foreach end
      $tbl .= '
        <tr>
          <td> </td>
          <td> </td>
          <td> </td>
          <td> TOTALS </td>
          <td align="right">' . number_format($totalBasic, 2) . '</td>
          <td align="right">' . number_format($totalGsis, 2) . '</td>
          <td align="right">' . number_format($totalWtax, 2) . '</td>
          <td align="right">' . number_format($totalLoans, 2) . '</td>
          <td align="right">' . number_format($totalDed, 2) . '</td>
          <td align="right">' . number_format($totalNetPay, 2) . '</td>
          <td >' . '' . '</td>
        </tr>
      ';
    $tbl .= '
      <table>
        <tr>
          <td> </td> 
        </tr>
        <tr>
          <td align="left" style="width: 10%">Prepared by:</td>
          <td align="left">' . $office['name'] . '</td>
        </tr>
      </table>
    ';
    $tbl .='
          </tbody>
        </table>
      </div>
    ';

    return $tbl;
  }

  function employee_datatable()
  {
    $employees = fetch_payslip();
    // $offices = fetch_offices();
    // foreach ($offices as $office) {
      $tbl = '';
      
      $tbl = '
          <table cellpadding="5" border="0">
            <tr>
              <td colspan="3" align="center"><u><h1>EMPLOYEE REPORT</b></h1></td>
            </tr>
          </table>
      ';

      // $employees = fetch_all_office_employees($office['id']);
      $tbl .= '  
          <div style="width:100%;"> 
            <table  cellpadding="5" cellspacing="1" border="1" style="text-align:center;"> 
              <tr >
                <th style="width:5%;">NO.</th>
                <th style="width:15%;">NAME</th>
                <th style="width:10%;">POSITION</th>
                <th style="width:10%;">OFFICE</th>
                <th style="width:7%;">PAY CODE</th>
                <th style="width:8%;">BASIC</th>
                <th style="width:8%;">GSIS</th>
                <th style="width:8%;">WTAX</th>
                <th style="width:8%;">LOANS</th>
                <th style="width:10%;">TOTALDEC</th>
                <th style="width:5%;">NETPAY</th>
              </tr>
          <tbody>
        ';
      foreach ($employees as $employee) {
        
        $tbl .= '
            <tr>
              <td align="left">' . $employee['id'] . '</td>
              <td align="left">' . $employee['full_name'] . '</td>
              <td align="left">' . $employee['position'] . '</td>
              <td align="left">' . $employee['name'] . '</td>
              <td align="center">' . $employee['pay_code'] . '</td>
              <td align="center">' . $employee['basic'] . '</td>
              <td align="center">' . $employee['gsis'] . '</td>
              <td align="center">' . $employee['wtax'] . '</td>
              <td align="center">' . $employee['loans'] . '</td>
              <td align="center">' . $employee['totaldec'] . '</td>
              <td align="center">' . $employee['net_pay'] . '</td>
            </tr>
          ';
      }
      
      $tbl .= '
              </tbody>
            </table>
          </div>
        ';
      return $tbl;
    // }
  }

  function office_datatable()
  {
    $offices = fetch_offices();

    $tbl = '';
    $tbl = '
        <table cellpadding="5" border="0">
          <tr>
            <td colspan="3" align="center"><u><h1>OFFICE REPORT</b></h1></td>
          </tr>
        </table>
    ';

    $tbl .= '  
        <div style="width:100%;"> 
          <table  cellpadding="5" cellspacing="1" border="1" style="text-align:center;"> 
            <tr >
              <th style="width:70%;">OFFICE</th>
              <th style="width:30%;">PAY CODE</th>
            </tr>
        <tbody>
      ';

    foreach ($offices as $office) {
      $tbl .= '
          <tr>
            <td align="left">' . $office['name'] . '</td>
            <td align="center">' . $office['pay_code'] . '</td>
            <td align="left">' . '' . '</td>
          </tr>
        ';
    }
    $tbl .= '
            </tbody>
          </table>
        </div>
      ';
    return $tbl;
  }

  function payslip_datatable($office_id)
  {
    // $employees = fetch_office_employee($office_id);
    $employees = get_payslip_pdf($office_id);

    $tbl = '';
    foreach ($employees as $employee) {
      $tbl .= '
          <table style="width:50%">
            <tr>
              <td align="center">
                <h1>PAYSLIP</h1>
              </td>
            </tr>
          </table>
          <p></p>
          <table>
            <tr >
              <td align="left" style="width:10%">Name</td>
              <td align="left">' . $employee['full_name'] . '</td>
            </tr>
            <tr >
              <td>Position</td>
              <td>' . $employee['position'] . '</td>
            </tr>
            <tr >
              <td>Office</td>
              <td>' . $employee['name'] . '</td>
            </tr>
          </table>
          <p></p>
          <table border="0" id="gross" style="margin-bottom:50px;width:50%">
            <tbody>
              <tr>
                <td>Basic</td>
                <td align="right">' . $employee['basic'] . '</td>
              </tr>
            </tbody>
          </table>
          <p></p>
          <table border="0" id="deduction" style="width:50%">
            <tbody>
              <tr>
                <td align="center">Deduction</td>
                <td></td>
              </tr>
              <tr>
                <td>GSIS</td>
                <td align="right">' . $employee['gsis'] . '</td>
              </tr>
              <tr>
                <td>WTAX</td>
                <td align="right">' . $employee['wtax'] . '</td>
              </tr>
              <tr>
                <td>LOANS</td>
                <td align="right">' . $employee['loans'] . '</td>
              </tr>
              <tr>
                <td>TOTAL</td>
                <td></td>
              </tr>
            </tbody>
          </table>
        <p></p>
          <table border="0" id="netpay" style="width:50%">
            <tbody>
              <tr>
                <td>Net Pay</td>
                <td align="right">' . $employee['net_pay'] . '</td>
              </tr>
            </tbody>
          </table>
        <p></p>
        <p style="page-break-after: always;">&nbsp;</p>
      '; 
    }
    
    return $tbl;
  }