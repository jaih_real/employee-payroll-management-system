<?php

ob_start();

// only require the file once
// require_once throw fatal error and stop the code
// include throw a warning and continue code execution 
require_once '../public/config/db_config.php';
require_once '../public/header.php';
require_once '../public/sidebar.php';

require_once 'crud/select.php';
require_once 'crud/update.php';
require_once 'crud/function.php';

$alert_msg = '';

if (isset($_GET['id'])) {
  # code...
  $office_id = $_GET['id'];
  $contact = get_office_by_id($office_id);
  foreach ($contact as $key => $value) {
    # code...
    $$key = $value;
  }
}
if (isset($_POST['update_payroll'])) {
  # code...
  $data = array(
    'name' => $_POST['name'],
    'id' => $office_id
  );

  # code...
  $payroll_updated = update_payroll($data);
  if ($payroll_updated) {
    $alert_msg .= ' 
        <div class="new-alert new-alert-success alert-dismissible">
            <i class="icon fa fa-info"></i>
            Data Updated.
        </div>
      ';
    header('location: payroll.php');
    exit;
  } else {
    $alert_msg .= ' 
        <div class="new-alert new-alert-warning alert-dismissible">
          <i class="icon fa fa-info"></i>
          Invalid Update.
        </div>
      ';
  }
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 style="border-bottom: 3px solid #bbb;">
      <b>UPDATE PAYROLL</b>
      <!-- <small>Version 2.0</small> -->
    </h1>
    <?php echo $alert_msg; ?>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Payroll Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" action="<?php htmlspecialchars("PHP_SELF"); ?>">
            <div class="box-body">

              <?php echo $alert_msg; ?>

              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Pay Code</label>
                </div>
                <div class="col-md-10">
                  <input type="text" disabled class="form-control" name="pay_code" placeholder="Pay Code" value="<?= $pay_code ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Office Name</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="name" placeholder="Office Name" value="<?= $name ?>" required>
                </div>
              </div><br>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <input type="submit" name="update_payroll" class="btn btn-primary" value="Save">
              <a href="offices">
                <input type="button" name="back" class="btn btn-default" value="Back">
              </a>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-1"></div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php
require_once '../public/footer.php';
?>