<?php

ob_start();

// only require the file once
// require_once throw fatal error and stop the code
// include throw a warning and continue code execution 

require_once '../public/config/db_config.php';
require_once '../public/header.php';
require_once '../public/sidebar.php';

require_once 'crud/select.php';
require_once 'crud/update.php';
require_once 'crud/function.php';

$alert_msg = '';


if (isset($_GET['id'])) {
  # code...
  $employee_id = $_GET['id'];
  $employee = get_payslip_by_id($employee_id);
  foreach ($employee as $key => $value) {
    # code...
    $$key = $value;
  }
}

if (isset($_POST['update_payslip'])) {
  # code...

  $data = array(
    'full_name' => $_POST['full_name'],
    'position' => $_POST['position'],
    'basic' => $_POST['basic'],
    'gsis' => $_POST['gsis'],
    'wtax' => $_POST['wtax'],
    'loans' => $_POST['loans'],
    'totaldec' => $_POST['totaldec'],
    'net_pay' => $_POST['net_pay'],
    'id' => $employee_id
  );

  $payslip_updated = update_payslip($data);
  if ($payslip_updated) {
    header('Location: payslip');
    exit;
  } else {
    $alert_msg .= ' 
          <div class="new-alert new-alert-warning alert-dismissible">
              <i class="icon fa fa-info"></i>
              Invalid Update.
          </div>
      ';
  }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 style="border-bottom: 3px solid #bbb;">
      <b>UPDATE EMPLOYEE PAYSLIP</b>
      <!-- <small>Version 2.0</small> -->
    </h1>
    <?php echo $alert_msg; ?>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Employee Payslip Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" action="<?php htmlspecialchars("PHP_SELF"); ?>">
            <div class="box-body">
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>ID</label>
                </div>
                <div class="col-md-10">
                  <input disabled type="text" class="form-control" name="pay_code" placeholder="Pay Code" value="<?= $id ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Pay Code</label>
                </div>
                <div class="col-md-10">
                  <input disabled type="text" class="form-control" name="pay_code" placeholder="Pay Code" value="<?= $pay_code ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Name</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="full_name" placeholder="Name" value="<?= $full_name ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Position</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="position" placeholder="Position" value="<?= $position ?>">
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Basic</label>
                </div>
                <div class="col-md-10">
                  <input type="number" class="form-control" name="basic" placeholder="Basic" value="<?= $basic ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>GSIS</label>
                </div>
                <div class="col-md-10">
                  <input type="number" class="form-control" name="gsis" placeholder="GSIS" value="<?= $gsis ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>WTax</label>
                </div>
                <div class="col-md-10">
                  <input type="number" class="form-control" name="wtax" placeholder="WTax" value="<?= $wtax ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Loans</label>
                </div>
                <div class="col-md-10">
                  <input type="number" class="form-control" name="loans" placeholder="Loans" value="<?= $loans ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Totaldec</label>
                </div>
                <div class="col-md-10">
                  <input type="number" class="form-control" name="totaldec" placeholder="Totaldec" value="<?= $totaldec ?>" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Net Pay</label>
                </div>
                <div class="col-md-10">
                  <input type="number" class="form-control" name="net_pay" placeholder="Net Pay" value="<?= $net_pay ?>" required>
                </div>
              </div><br>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <input type="submit" name="update_payslip" class="btn btn-primary" value="Save">
              <a href="payslip">
                <input type="button" name="back" class="btn btn-default" value="Back">
              </a>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-1"></div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
require_once '../public/footer.php';
?>