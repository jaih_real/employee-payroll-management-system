<?php

// only require the file once
// require_once throw fatal error and stop the code
// include throw a warning and continue code execution 
require_once '../public/config/db_config.php';
require_once '../public/header.php';
require_once '../public/sidebar.php';
require_once 'crud/function.php';
require_once 'crud/insert.php';
require_once 'crud/select.php';

$alert_msg = '';


if (isset($_POST['insert_employee'])) {
  $totalDec =  (float)$_POST['gsis'] +  (float)$_POST['wtax'] +  (float)$_POST['loans'];
  $netPay   =  (float)$_POST['basic'] - $totalDec;


  $data = array(
    'office_id' => $_POST['office'],
    'full_name' => $_POST['full_name'],
    'position' => $_POST['position'],
    'basic' => $_POST['basic'],
    'gsis' => $_POST['gsis'],
    'wtax' => $_POST['wtax'],
    'loans' => $_POST['loans'],
    'totaldec' => $totalDec,
    'net_pay' => $netPay
  );
  $insert_data = insert_employee($data);
  if ($insert_data) {
    header('location: update_employee.php?id=' . $insert_data);
    $alert_msg .= ' 
          <div class="new-alert new-alert-success alert-dismissible">
              <i class="icon fa fa-info"></i>
              Data Inserted.
          </div>
      ';
  } else {
    $alert_msg .= ' 
          <div class="new-alert new-alert-warning alert-dismissible">
              <i class="icon fa fa-info"></i>
              Invalid Data.
          </div>
      ';
  }
}

$offices = fetch_offices();
$positions = fetch_positions();
// $positions = ['Position 1', 'Position 1'];


?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Employee
      <!-- <small>Version 2.0</small> -->
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Employee Details</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" action="<?php htmlspecialchars("PHP_SELF"); ?>">
            <div class="box-body">

              <?php echo $alert_msg; ?>


              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Full Name</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="full_name" placeholder="Full name" value="" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Office</label>
                </div>
                <div class="col-md-10">
                  <select name="office" id="office" class="form-control" required>
                    <option selected disabled value=""> Select </option>
                    <?php foreach ($offices as $office) { ?>
                      <option value="<?= $office['id'] ?>"><?= $office['name'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Position</label>
                </div>
                <div class="col-md-10">
                  <select name="position" id="position" class="form-control" required>
                    <option selected disabled value=""> Select </option>
                    <?php foreach ($positions as $position) { ?>
                      <option value="<?= $position['id'] ?>"><?= $position['name'] ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Basic</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="basic" placeholder="Basic" value="" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>GSIS</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="gsis" placeholder="GSIS" value="" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>WTax</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="wtax" placeholder="WTax" value="" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Loans</label>
                </div>
                <div class="col-md-10">
                  <input type="text" class="form-control" name="loans" placeholder="Loans" value="" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Totaldec</label>
                </div>
                <div class="col-md-10">
                  <input disabled type="number" class="form-control" name="totaldec" placeholder="Totaldec" value="" required>
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2" style="text-align: right;padding-top: 5px;">
                  <label>Net Pay</label>
                </div>
                <div class="col-md-10">
                  <input disabled type="number" class="form-control" name="net_pay" placeholder="Net Pay" value="" required>
                </div>
              </div><br>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <input type="submit" name="insert_employee" class="btn btn-primary" value="Save">
              <a href="employees">
                <input type="button" name="back" class="btn btn-default" value="Back">
              </a>
            </div>
          </form>
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-1"></div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<?php
require_once '../public/footer.php';
?>