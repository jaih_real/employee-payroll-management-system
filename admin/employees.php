<?php

// only require the file once
// require_once throw fatal error and stop the code
// include throw a warning and continue code execution 

require_once '../public/config/db_config.php';
require_once '../public/header.php';
require_once '../public/sidebar.php';

require_once 'crud/select.php';
require_once 'crud/delete.php';

$alert_msg = '';

// $user_id  = $_SESSION['id'];

if (isset($_POST['delete_employee'])) {
  # code...
  $delete  = delete_employee($_POST['employee_id']);
  if ($delete) {
    $alert_msg .= ' 
      <div class="new-alert new-alert-warning alert-dismissible">
          <i class="icon fa fa-info"></i>
          Employee deleted!
      </div>
    ';
  }
}

$employees = fetch_employees();

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1 style="border-bottom: 3px solid #bbb; margin-bottom:10px;">
      <b>EMPLOYEES</b>
    </h1>
    <center>
      <?php
      if (isset($_POST['delete_employee'])) {
        echo $alert_msg;
      }
      ?>
    </center>
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Employee Details</h3>
            <a href="../reports/pdf.php?key=employees" style="float: right" target="_blank">
              <button class="btn btn-sm btn-primary btn-block margin-bottom">
                Generate PDF
              </button>
            </a>
            <a href="add_employee" style="float: right; margin-right: 10px">
              <button class="btn btn-sm btn-primary btn-block margin-bottom">
                Add Employee
              </button>
            </a>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" method="post" action="<?php htmlspecialchars("PHP_SELF"); ?>">
            <div class="box-body">
              <table id="dataTable" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Position</th>
                    <th>Updated At</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($employees as $employee) {
                  ?>
                    <tr>
                      <td><?= $employee['full_name'] ?></td>
                      <td><?= $employee['position'] ?></td>
                      <td><?= $employee['updated_at'] ?></td>
                      <td>
                        <a class="btn btn-outline-success btn-xs" href="update_employee.php?id=<?= $employee['id'] ?>"><i class="fa fa-check-square-o"></i>
                          edit</a>
                        &nbsp;
                        <button class="btn btn-outline-danger btn-xs" data-role="confirm_delete_employee" data-employeeid="<?= $employee["id"] ?>"><i class="fa fa-trash-o"></i></button>
                        &nbsp;
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-1"></div>
    </div>

  </section>
  <!-- /.content -->

</div>
<!-- /.content-wrapper -->

<!-- modals here -->
<!-- modal here delete -->
<div class="modal fade" id="delete_employee-form" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <form method="POST" action="<?php htmlspecialchars("PHP_SELF") ?>">
        <div class="modal-body">
          <div class="box-body">
            <div class="form-group">
              <label>Delete Record?</label>
              <input type="hidden" name="employee_id" id="employee_id" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left bg-olive" data-dismiss="modal">No</button>
          <input type="submit" name="delete_employee" class="btn btn-danger" value="Yes">
        </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php
require_once '../public/footer.php';
?>