-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 23, 2021 at 06:42 AM
-- Server version: 5.7.24
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee_payroll_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `office_id` int(11) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `basic` decimal(60,2) DEFAULT NULL,
  `gsis` decimal(60,2) DEFAULT NULL,
  `wtax` decimal(60,2) DEFAULT NULL,
  `loans` decimal(60,2) DEFAULT NULL,
  `totaldec` decimal(60,2) DEFAULT NULL,
  `net_pay` decimal(60,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `office_id`, `full_name`, `position`, `basic`, `gsis`, `wtax`, `loans`, `totaldec`, `net_pay`, `created_at`, `updated_at`) VALUES
(1, 8, 'Jaih Real Sasing', 'Accountant', '99001.50', '123.00', '123.00', '123.00', '369.00', '98632.50', '2021-06-21 13:15:04', '2021-06-23 05:32:40'),
(13, 9, 'Test 7', 'Senior Software Developer', '99000.50', '9000.00', '2000.00', '2000.00', '13000.00', '86000.00', '2021-06-22 08:04:56', '2021-06-22 08:04:56'),
(17, 11, 'new', 'Software Dev', '99000.50', '9000.00', '2000.00', '2000.25', '13000.25', '86000.25', '2021-06-22 08:13:16', '2021-06-22 08:13:16'),
(18, 8, 'new 2', 'Software Dev', '99000.50', '9000.00', '2000.00', '2000.25', '13000.25', '86000.25', '2021-06-22 08:27:48', '2021-06-22 08:27:48'),
(22, 8, 'new 3', 'Software Dev', '99000.50', '9000.00', '2000.00', '2000.25', '13000.25', '86000.25', '2021-06-22 08:37:05', '2021-06-22 08:37:05'),
(23, 9, 'new5', 'Software Dev', '99000.50', '9000.00', '2000.00', '2000.25', '13000.25', '86000.25', '2021-06-22 08:37:50', '2021-06-22 08:40:48'),
(24, 8, 'test 8', 'Software Dev', '99000.50', '9000.00', '2000.00', '2000.25', '13000.25', '86000.25', '2021-06-23 03:35:42', '2021-06-23 03:35:42');

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` int(10) NOT NULL,
  `pay_code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `offices`
--

INSERT INTO `offices` (`id`, `pay_code`, `name`, `created_at`, `updated_at`) VALUES
(8, '5', 'HUMAN RESOURCE', '2021-06-21 08:36:27.025967', '2021-06-21 08:36:27.025967'),
(9, '1', 'OPERATIONS', '2021-06-21 08:38:55.955860', '2021-06-23 02:57:40.629573'),
(10, '11', 'ACCOUNTING', '2021-06-22 05:10:23.677929', '2021-06-22 05:10:23.677929'),
(11, '12', 'HUMAN RESOURCE', '2021-06-22 05:12:14.413292', '2021-06-22 06:43:04.681026');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Accountant', '2021-06-23 05:18:21', '2021-06-23 05:18:21'),
(2, 'Treasurer', '2021-06-23 05:18:21', '2021-06-23 05:18:21'),
(3, 'Budget Officer', '2021-06-23 05:18:44', '2021-06-23 05:18:44'),
(4, 'Collector', '2021-06-23 05:18:44', '2021-06-23 05:18:44');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
