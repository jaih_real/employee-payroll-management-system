<?php
	
	session_start();

	if ( ! isset($_SESSION['id'])) {
	  # code...
	  header('location: ../index.php');
	}


	// should be use above not inside if condition
	// spreadsheet class
	use PhpOffice\PhpSpreadsheet\Spreadsheet;
	// xlsx writer
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
	// load iofactory
	use PhpOffice\PhpSpreadsheet\IOFactory;

	// only require the file once
	// require_once throw fatal error and stop the code
	// include throw a warning and continue code execution 
	require_once '../public/config/db_config.php';
	require_once '../admin/crud/select.php';
	require_once '../vendor/autoload.php';

	if ( ! isset($_GET['key']) ) {
	  # code...

		exit();

	}
	else{
		$indicator = $_GET['key'];
	}


	switch ($indicator) {
		case 'contacts':
			generate_contacts();
			break;
		case 'users':
			
			break;
		default:
			// code...
			echo 'invalid data';
			break;
	}


	// init function --------------------------------------------------

	// clean buffer if not empty
	function clean_buffer(){
		if(ob_get_length() > 0) {
		    ob_end_clean();
		}
	}


	//excel default function
	function excel_header($name){
		$date_time = date("Y-m-d");
		header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		header('Content-Disposition: attachment;filename="'.$name.'-'.$date_time.'.xlsx"'); //filename of your excel file

	}


	// data to excel functions ----------------------------------------	
	// accounts

	function generate_contacts(){

		// instance of spreadsheet
		$spreadsheet = new Spreadsheet();

		// Rename worksheet
		$spreadsheet->getActiveSheet()->setTitle('Contacts');

		//set cell name
		$spreadsheet->getActiveSheet(0)->setCellValue('A1', 'First Name');
		$spreadsheet->getActiveSheet(0)->setCellValue('B1', 'Middle Name');
		$spreadsheet->getActiveSheet(0)->setCellValue('C1', 'Last Name');
		$spreadsheet->getActiveSheet(0)->setCellValue('D1', 'Email');
		$spreadsheet->getActiveSheet(0)->setCellValue('E1', 'Contact Number');


		//set the size and dimension of our cell whatever the size also of the cellname
		foreach (range('A', 'E') as $cols) {
			$spreadsheet->getActiveSheet()
				->getColumnDimension($cols)
				->setAutoSize(true);
		}



		//we start to loop on row 2 of our excel file
		$row_count = 3;	


		$contacts = fetch_contacts();

		foreach($contacts as $contact):
			
			$spreadsheet->getActiveSheet()->setCellValue('A'.$row_count, $contact['first_name']);
			$spreadsheet->getActiveSheet()->setCellValue('B'.$row_count, $contact['middle_name']);
			$spreadsheet->getActiveSheet()->setCellValue('C'.$row_count, $contact['last_name']);
			$spreadsheet->getActiveSheet()->setCellValue('D'.$row_count, $contact['email']);
			$spreadsheet->getActiveSheet()->setCellValue('E'.$row_count, $contact['contact_number']);

			$row_count += 1;

		endforeach;

		excel_header('contact-summary');
		//clean buffer
		clean_buffer();
		// create iofactory object
		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		// output file
		$writer->save('php://output');
		exit;


	}


