<?php

	// only require the file once
	// require_once throw fatal error and stop the code
	// include throw a warning and continue code execution 

	require_once '../public/config/db_config.php';
	require_once '../assets/plugins/TCPDF/tcpdf.php';
	require_once '../admin/crud/select.php';
	require_once '../admin/crud/function.php';
	require_once '../admin/crud/data_table.php';
	require_once '../admin/crud/subject_line.php';

	// Extend the TCPDF class to create custom Header and Footer
	class MYPDF extends TCPDF {

	  //Page header
	  public function Header() {
			// Logo

			$image_file = '../assets/theme/img/danao.png'; // *** Very IMP: make sure this image is available on given path on your server
			$image_file2 = '../assets/theme/img/danao.png'; // *** Very IMP: make sure this image is available on given path on your server
			// $this->Image($image_file,15,6,30);
			// first number is space from left
			// second number is space from above
			// image height?


			// $headerData = $this->getHeaderData();
			// $this->SetFont('helvetica', 'B', 10);
			// $this->writeHTML($headerData['string']);

			$this->Image($image_file, 13, 10, 25, '', 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);
			// Set font

			$this->SetFont('helvetica', 'C', 26);
			$this->Ln(5);
			$this->Cell(180, 0, 'Danao City Government', 0, false, 'C', 0, '', 0, false, 'M', 'M');
			// $this->Ln(5);
			// $this->Cell(180, 0, 'HDa. Sta. Ana Road (C2 Road), Brgy. Palampas, San Carlos City', 0, false, 'C', 0, '', 0, false, 'M', 'M');
			$this->Image($image_file2, 260, 10, 25, '', 'png', '', 'T', false, 300, '', false, false, 0, false, false, false);    
			$this->Ln(10);
			// output the HTML content
			// $pdf->writeHTML(requesting_data_part(), true, false, true, false, '');
			// $this->Cell(250, 50, 'Company tag line here', 0, false, 'C', 0, '', 0, false, 'M', 'M');
			// We need to adjust the x and y positions of this text ... first two parameters
	        
	  }

	  // Page footer
	  public function Footer() {
			// // Position at 25 mm from bottom
			// $this->SetY(-15);
			// // Set font
			// $this->SetFont('helvetica', 'I', 8);
			
			// $this->Cell(0, 0, '', 0, 0, 'C');
			// $this->Ln();
			// $this->Cell(0,0,'www.clientsite.com - T : +91 1 123 45 64 - E : info@clientsite.com', 0, false, 'C', 0, '', 0, false, 'T', 'M');
			
			// // Page number
			// $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	  }
	}

	echo '<pre>';
	echo format($_GET['id']);
	echo '</pre>';
	if ( ! isset($_GET['key']) ) {
		exit();
	}
	else{
		$indicator = $_GET['key'];
	}


	switch ($indicator) {
		case 'employees':
			generate_employees();
			break;
		case 'offices':
			generate_office();
			break;
		case 'payroll':
			generate_payroll();
			break;
		case 'payslip':
			generate_payslip();
			break;
		default:
			// code...
			echo 'invalid data';
			break;
	}

	// defined functions ------------------------------------------------
	// clean buffer
	// clean buffer if not empty
	function clean_buffer(){
		if(ob_get_length() > 0) {
		    ob_end_clean();
		}
	}

	function signature_line(){

		// $html = '';
		// $html .= '
		// 		<br><br><br><br>
		// 		<table cellpadding="5" border="0" nobr="true">
		// 			<tr >
		// 				<td colspan="3" align="left">Noted By:</td>
		// 			</tr>
		// 			<tr>
		// 				<td align="center">
		// 					<u><b>MA. CONSUELO JANICE R. CARMONA</b></u>
		// 					<br>
		// 					Administrative Officer
		// 				</td>
		// 			</tr>
		// 			<tr>
		// 				<td></td>
		// 			</tr>
		// 			<tr>
		// 				<td>Requested By:</td>
		// 				<td>Issued By:</td>
		// 				<td>Received By:</td>
		// 			</tr>
		// 			<tr align="center">
		// 				<td>
		// 					<u><b>CRISPINA S. AGUANTA</b></u>
		// 					<br>
		// 					Pharmacy Aide
		// 				</td>
		// 				<td>
		// 					<u><b>KENNETH B. CORDOVA</b></u>
		// 					<br>

		// 				</td>
		// 				<td>
		// 					<u><b>CRISPINA S. AGUANTA</b></u>
		// 					<br>
		// 					Pharmacy Aide
		// 				</td>
		// 			</tr>
		// 			<tr>
		// 				<td></td>
		// 				<td></td>
		// 				<td></td>
		// 			</tr>
		// 			<tr>
		// 				<td style="width:30%">
		// 				</td>
		// 				<td style="width:30%">
		// 				</td>
		// 				<td style="width:40%">
		// 					Approved by:
		// 				</td>
		// 			</tr>
		// 			<tr>
		// 				<td style="width:30%">
		// 				</td>
		// 				<td style="width:30%">
		// 				</td>
		// 				<td style="width:40%" align="center">
		// 					<u><b>ARCHILLES A. PONFERRADA, MD., RN</b></u>
		// 					<br>
		// 					City Govt. Dept. Head I
		// 				</td>
		// 			</tr>

		// 	    </table>
		// ';

		// return $html;
	}

	//data to pdf functions ---------------------------------------------
	function generate_employees(){

		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nilesh Zemse');
		$pdf->SetTitle('List of Employees');
		$pdf->SetSubject('List of Employees');
		$pdf->SetKeywords('PDF, Summary');


		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		//set some language-dependent strings
		// $pdf->setLanguageArray($l);

		$pdf->setPageOrientation('L');
		// ---------------------------------------------------------

		// set font
		// $pdf->SetFont('times', '', 15); 
		$pdf->SetFont('helvetica', '', 8);

		// *** Very IMP: Please use times font, so that if you send this pdf file in gmail as attachment and if user
		//opens it in google document, then all the text within the pdf would be visible properly.
		// add a page

		$pdf->AddPage();
		$pdf->Ln(15);
		// output the HTML content
		// $pdf->writeHTML(employee_subjectline(), true, false, true, false, ''); // configuration in subject_line.php function payroll_subjectline()

		// output the HTML content
		$pdf->writeHTML(employee_datatable(), true, false, true, false, ''); // configuration in data_table.php function payroll_datatable()

		// $pdf->Ln(10);
		// output the HTML content
		// $pdf->writeHTML(signature_line(), true, false, true, false, '');

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		// reset pointer to the last page
		$pdf->lastPage();

		clean_buffer();
		// ---------------------------------------------------------

		//Close and output PDF document
		$pdf_file_name = 'employee_list.pdf';
		$pdf->Output($pdf_file_name, 'I');
	}

	function generate_office(){
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nilesh Zemse');
		$pdf->SetTitle('List of Offices');
		$pdf->SetSubject('List of Offices');
		$pdf->SetKeywords('PDF, Summary');


		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		//set some language-dependent strings
		// $pdf->setLanguageArray($l);

		$pdf->setPageOrientation('L');
		// ---------------------------------------------------------

		// set font
		// $pdf->SetFont('times', '', 15); 
		$pdf->SetFont('helvetica', '', 8);

		// *** Very IMP: Please use times font, so that if you send this pdf file in gmail as attachment and if user
		//opens it in google document, then all the text within the pdf would be visible properly.
		// add a page

		$pdf->AddPage();
		$pdf->Ln(15);
		// output the HTML content
		// $pdf->writeHTML(employee_subjectline(), true, false, true, false, ''); // configuration in subject_line.php function payroll_subjectline()

		// output the HTML content
		$pdf->writeHTML(office_datatable(), true, false, true, false, ''); // configuration in data_table.php function payroll_datatable()

		// $pdf->Ln(10);
		// output the HTML content
		// $pdf->writeHTML(signature_line(), true, false, true, false, '');

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
		// reset pointer to the last page
		$pdf->lastPage();

		clean_buffer();
		// ---------------------------------------------------------

		//Close and output PDF document
		$pdf_file_name = 'office_list.pdf';
		$pdf->Output($pdf_file_name, 'I');
	}

	function generate_payroll(){
		$office_id = $_GET['id'];
		
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nilesh Zemse');
		$pdf->SetTitle('Employee Payroll');
		$pdf->SetSubject('Employee Payroll');
		$pdf->SetKeywords('PDF, Summary');


		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		//set some language-dependent strings
		// $pdf->setLanguageArray($l);

		$pdf->setPageOrientation('L');

		// ---------------------------------------------------------

		// set font
		// $pdf->SetFont('times', '', 15); 
		$pdf->SetFont('helvetica', '', 8);

		// *** Very IMP: Please use times font, so that if you send this pdf file in gmail as attachment and if user
		//opens it in google document, then all the text within the pdf would be visible properly.
		// add a page


		$pdf->AddPage();
		$pdf->Ln(15);
		// output the HTML content
		$pdf->writeHTML(payroll_subjectline($office_id), true, false, true, false, ''); // configuration in subject_line.php function payroll_subjectline()


		// output the HTML content
		$pdf->writeHTML(payroll_datatable($office_id), true, false, true, false, ''); // configuration in data_table.php function payroll_datatable()



		$pdf->Ln(10);
		// output the HTML content
		$pdf->writeHTML(signature_line(), true, false, true, false, '');

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// reset pointer to the last page
		$pdf->lastPage();

		clean_buffer();
		// ---------------------------------------------------------

		//Close and output PDF document
		$pdf_file_name = 'employee_payroll.pdf';
		$pdf->Output($pdf_file_name, 'I');
	}

	function generate_payslip(){

		$office_id = $_GET['id'];
		
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nilesh Zemse');
		$pdf->SetTitle('Employee Payslip');
		$pdf->SetSubject('Employee Payslip');
		$pdf->SetKeywords('PDF, Summary');


		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		//set some language-dependent strings
		// $pdf->setLanguageArray($l);

		// $pdf->setPageOrientation('L');

		// ---------------------------------------------------------

		// set font
		// $pdf->SetFont('times', '', 15); 
		$pdf->SetFont('helvetica', '', 8);

		// *** Very IMP: Please use times font, so that if you send this pdf file in gmail as attachment and if user
		//opens it in google document, then all the text within the pdf would be visible properly.
		// add a page


		$pdf->AddPage();
		$pdf->Ln(15);
		// output the HTML content
		// $pdf->writeHTML(payslip_subjectline($employee_id), true, false, true, false, ''); // configuration in subject_line.php function payroll_subjectline()


		// output the HTML content
		$pdf->writeHTML(payslip_datatable($office_id), true, false, true, false, ''); // configuration in data_table.php function payroll_datatable()



		$pdf->Ln(10);
		// output the HTML content
		// 	$pdf->writeHTML(signature_line(), true, false, true, false, '');

		// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		// reset pointer to the last page
		$pdf->lastPage();

		clean_buffer();
		// ---------------------------------------------------------

		//Close and output PDF document
		$pdf_file_name = 'employee_payslip.pdf';
		$pdf->Output($pdf_file_name, 'I');
	}